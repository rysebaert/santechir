---
title: "Droits d'utilisation et description des données consolidées"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


Cette page décrit les données consolidées utilisées dans les différents scripts du site Web. Elles correspondent au fichier de sortie issu de la partie **[Préparation des données](01_preparation.html)** et intégré dans le fichier geom.gpkg. 

<br>


# Informations générales

## Accès 

Le geopackage contenant l'ensemble des couches géographiques utiles est téléchargeable [ici](https://gitlab.huma-num.fr/rysebaert/santechir/-/blob/master/data/geom.gpkg).


## Date de création du jeu de données  

Février 2021


## Date des métadonnées  

Février 2021

## Points de Contact
<p>
- **Nom** : Ronan Ysebaert
- **Role** : Point de contact 
- **Organisation** : UMS RIATE, Université de Paris
- **email** : ronan.ysebaert@cnrs.fr
</p>
<p>
- **Nom** : Timothée Giraud
- **Role** : Point de contact
- **Organisation** : UMS RIATE, CNRS  
- **email** : timothee.giraud@cnrs.fr
<p>
- **Nom** : Hugues Pécout
- **Role** : Point de contact
- **Organisation** : FR CIST, CNRS
- **email** : hugues.pecout@cnrs.fr
</p>
<p>
- **Nom** : Benoit Conti
- **Role** : Point de contact
- **Organisation** : ENPC
- **email** : benoit.conti@enpc.fr
</p>


## Conditions d'utilisation

Ce jeu de données et ces scripts sont déposés sous la licence [Creative Commons paternité - usage non commercial - partage à l'identique 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/).

Vous êtes libre de copier, distribuer, transmettre et adapter les informations disponibles depuis ce site Web, à condition que vous créditiez le projet. Si vous modifiez ou utilisez nos données dans d’autres œuvres dérivées, vous ne pouvez distribuer celles-ci que sous la même licence. Vous **n'êtes pas autorisé à faire un usage commercial** de cette base de données.


## Comment citer cette ressource ?

"RIATE - ENPC - IRDES - Géographie-Cités, Base de données accessibilité aux soins de chirurgie, 2021"

<br>
<br>



# Données de référence consolidées utilisées dans l'étude

## Étendue spatiale et SCR

L'emprise des couches géographiques d'entrée correspond aux limites de la France métropolitaine. 

- Xmin : 99216.86
- Xmax : 1242426
- Ymin : 6049646
- Ymax : 7110480

Système de Coordonnées de Référence (SCR) : EPSG 2154 - RGF93_Lambert_93.

Voici le détail des couches disponibles dans le fichier *geom.gpkg* et leur emprise respective. Celles-ci sont utilisées tantôt à des fins de représentation cartographique, tantôt à des fins d'analyse.

```{r, eval = TRUE, echo=FALSE}
library(readxl)
library(flextable)

meta <- data.frame(read_excel("data-raw/meta.xlsx", sheet = "couches"))

myft <- flextable(meta)
myft <- theme_vanilla(myft)
myft <- autofit(myft)

myft
```

<br>

## Attributs des couches contenant de l'information statistique

Le fichier geom.gpkg contient un nombre important d'indicateurs systématiquement géoréfencés. Les tables ci-dessous décrivent leur code, leur libellé, leur origine et quelques informations additionnelles utiles pour comprendre leur méthode de construction (lié à la préparation des données).  

### Hôpitaux

Les données hospitalières sont contenues dans la couche **hop** du geopackage. Il s'agit d'une couche de points géoréférencée grâce à la librairie `banr`. 

```{r, eval = TRUE, echo=FALSE}
meta <- data.frame(read_excel("data-raw/meta.xlsx", sheet = "hop"))

myft <- flextable(meta)
myft <- theme_vanilla(myft)
myft <- autofit(myft)

myft
```

<br>

### Données socio-économiques

Les données socio-économiques proviennent de 7 tables produites par l'INSEE et sont non transformées. Il s'agit principalement de données de stock à deux années de référence : 1999 et 2016. Sont aussi associées des zonages (EPCI d'appartenance, zone d'emploi, tranche d'aire urbaine, etc). Il s'agit d'une couche de polygones.

```{r, eval = TRUE, echo=FALSE}
meta <- data.frame(read_excel("data-raw/meta.xlsx", sheet = "insee"))

myft <- flextable(meta)
myft <- theme_vanilla(myft)
myft <- autofit(myft)

myft
```

<br>

### Distances aux hôpitaux

12 indicateurs évaluant les distance-temps routières sont disponibles dans la couche `com_dist`. Ils permettent la mesure du temps routier nécessaire pour rejoindre le premier ou le second hôpital le plus proche depuis le chef-lieu (mairie) de chaque commune. Ces indicateurs se décomposent par type de structure hospitalière (privé, public ou indifférencié) et par année de référence (hôpitaux en 2000 ou 2018). A noter que les temps routier reposent sur la même matrice de distance pour 2000 ou 2018. Il est en effet impossible via OpenStreetMap de calculer ces temps routiers pour 2000. 

```{r, eval = TRUE, echo=FALSE}
meta <- data.frame(read_excel("data-raw/meta.xlsx", sheet = "dist"))

myft <- flextable(meta)
myft <- theme_vanilla(myft)
myft <- autofit(myft)

myft
```

<br>

### Capacités hospitalières

La couche `com_cap` permet de disposer de 62 indicateurs qui permettent de caractériser le nombre de places à temps partiel (moins de 24h) et lits d'hôpitaux (période supérieure à 24h) disponibles dans différents voisinages fonctionnels (disponibilité dans la commune, à moins de 15, 30, 45 et 60 minutes par la route). Les places et lits sont d'abord agrégées à la commune. Les distances sont évaluées du chef-lieu (mairies) des communes disposant d'hôpitaux au chef-lieu des communes environnantes. Ce nombre de places / lits sont décomposés selon les caractéristiques des hôpitaux : type de structure (privé, public ou indifférencié) et par année de référence. 

```{r, eval = TRUE, echo=FALSE}
meta <- data.frame(read_excel("data-raw/meta.xlsx", sheet = "cap"))

myft <- flextable(meta)
myft <- theme_vanilla(myft)
myft <- autofit(myft)

myft
```

