---
title: "Éloignement de l'offre d'hospitalisation pour des soins de chirurgie en France : Quels territoires ? Quelles populations ?"
author: Ronan Ysebaert[^2], Timothée Giraud[^2], Hugues Pecout[^3], Benoit Conti[^5], Sophie Baudet-Michel[^4],  Charlène Le Neindre[^1],  
date: <small>`r format(Sys.time(), '%d %B %Y')`</small>
bibliography: biblio.bib
link_citations: true
---


```{r setup, echo=FALSE, cache=FALSE}
library(knitr)
library(rmdformats)

## Global options
options(max.print="75")
opts_chunk$set(echo=TRUE,
               cache=TRUE,
               prompt=FALSE,
               tidy=TRUE,
               comment=NA,
               message=FALSE,
               warning=FALSE, 
               eval=FALSE)
opts_knit$set(width=75)
```


Ce site Web présente l’ensemble du code R mis en oeuvre pour produire l’ensemble des analyses de l’étude, synthétisés dans les communications suivantes (liste à fournir au final). 

Il est composé de plusieurs parties :

- [Préparer les données](01_preparation.html) : Partie dédiée à la préparation des données utiles à l'analyse (import des données brutes, sélection des variables, géocodage, calcul des distances communes / hôpitaux).
- [Analyses](02_analyses.html) : Dédiée aux analyses. 
- [Métadonnées](03_data.html) : Dédiée à la description des indicateurs, des sources des données et des possibilités de réutilisation de ce matériel. 
- [Suivi](04_to_do.html) : Suivi de projet / réunions (ordre du jour, compte-rendus).


<br>

# Contexte et problématique

Depuis plusieurs décennies, le système de santé français a enregistré des transformations qui ont abouti à une diminution du nombre de lits hospitaliers de chirurgie en court séjour et à une augmentation du nombre de places (hospitalisation de moins de 24h). Fermetures et ouvertures ont opéré dans un réseau de plus de 370 villes équipées (sur 792) (@chantal2020, p. 52) en lits et places de chirurgie. Entre 2000 et 2016 le nombre de villes équipées en soins de chirurgie est passé de 374 à 313. 63 aires urbaines ont perdu des équipements et deux aires en ont gagné (Noyal-Pontivy dans le Morbihan et Brignolles dans le Var).

Les fermetures de lits et les ouvertures de places ont résulté de la réorganisation de l’offre de soins notamment en raison de l’évolution des formes de prise en charge, ces dernières étant de plus en plus tournées vers des alternatives à l’hospitalisation à temps complet (i.e. pour des séjours d’une durée supérieure à 24h). Le développement de la chirurgie ambulatoire (i.e. pour des séjours d’une durée inférieure à 24h) a en effet été rendu possible pour certaines procédures grâce à des innovations en matière de technologies médicales et médicamenteuses, en particulier aux progrès en anesthésie. Structurellement, cela a impliqué l’ouverture de places d’hospitalisation à temps partiel et contribué à la fermeture de lits d’hospitalisation à temps complet.


*Tableau 1 - Nombre de villes, en 2000 et 2016, possédant un type d’équipement hospitalier (lit ou place) en chirurgie en fonction de statut juridique de l’établissement*

|                | Public_2000 | Privé_2000 | Public_Privé_2000 | Public_2016 | Privé_2016 | Public_Privé_2016 |
|----------------|:-----------:|:----------:|:-----------------:|:-----------:|:----------:|:-----------------:|
| Lits           |     179     |     13     |         93        |      7      |      3     |         5         |
| Places         |      4      |      0     |         4         |      20     |      5     |         21        |
| Lits et places |     139     |     235    |        277        |     248     |     188    |        287        |
*NB : Table à construire à partir des données au final*

Ces évolutions et les nombreuses fermetures ont eu des effets importants sur les conditions d’accès aux soins en France. Si en 2007, 1 % des communes étaient équipées en hôpitaux (@coldefy2011) et accessibles à 30 % de la population (dans sa commune de résidence), qu’en est-il aujourd’hui ? Si l’accès à l’hôpital de façon général n’est que très peu quantifié, il est plus souvent mesuré en fonction des spécialités. Tous les hôpitaux n’assurant pas toutes les types de chirurgie (certaines étant plus spécialisées et donc rares que d’autres), ce temps varie aussi fortement selon les spécialités : accessible dans sa commune à 17,8% de la population pour la chirurgie vasculaire, il ne l'est plus qu'à 10% pour la chirurgie cardiaque et 4% pour la chirurgie des grands brûlés (@coldefy2011). Un autre exemple sectoriel très étudié par la littérature, celui des maternités, qui ont connu de nombreuses fermetures à la suite du décret du 9 octobre 1998 (seules les maternités où plus de 300 accouchements par an sont pratiqués sont autorisées à exercer en obstétrique). Ce décret a conduit à une réduction de leur nombre de 20 % entre 2002 et 2012 (Sénat, 2015 > **ref à ajouter**), contribuant à une concentration spatiale de l’offre : en 2016 en France, 80 % des naissances ont eu lieu dans 200 communes seulement (Bellamy, 2017 > **ref à ajouter**). Ici encore, les conséquences sur les temps d’accès sont importantes : en Bourgogne, la fermeture de 3 maternités (entre 2000 et 2009) a conduit à une très légère augmentation du temps d’accès moyen (+4min) mais surtout à une variation forte du temps maximum (+21min) (@charreire2011) avec plus de nombreuses femmes vivant dans des communes où la maternité la plus proche est située à plus d’une heure de trajet.

Dans quelles mesures les différentes catégories sociales sont-elles affectées par ces fermetures de services ? De nombreux travaux de recherche dans le domaine de la santé tentent de quantifier les conséquences croisées (i) du niveau d’accès d’un professionnel de santé ou d’un établissement spécifique ; (ii) du statut socioéconomique mesuré au niveau des individus ou du lieu de résidence ; (iii) des résultats d’une pathologie particulière (stade du cancer, mortalité etc.). De plus, les méthodes développées pour mesurer ces deux premières facettes sont très variables d’une étude à l’autre : (i) le niveau d’accès est parfois mesuré à la structure la plus proche (en temps ou en distance ; à l’adresse ou au centroïde de la commune), parfois en fonction d’un ratio entre l’offre et la demande, parfois par une combinaison de ces deux aspects ; (ii) le statut économique est mesuré à partir d’indicateurs disponible au niveau individuel (profession et catégorie socioprofessionnelle, niveau de diplôme, revenu du ménage etc.) ou au niveau du quartier ou de la commune (pourcentage de personne sous un seuil de pauvreté par exemple). Par ailleurs, ces travaux ne permettent pas toujours de prendre en compte le critère du choix des individus du l’établissement effectif dans lequel les soins sont proposés. Cet argument est particulièrement important pour le cas de la France où les individus disposent du choix libre de leur lieu de soin. Ainsi, le plus proche n’est pas souvent le lieu pratiqué même s’il permet de rendre compte dans une certaine mesure des inégalités d’offres à disposition. Par exemple, Pilinkgton et al (2008) ont montré que si seulement 1/3 des femmes choisissent la maternité la plus proche de chez elles, cette proportion augmente à 85 % pour celles vivant à plus de 30km. Et dans le même temps, le choix d’une maternité à proximité dépend aussi des caractéristiques sociales : les femmes de ménages ouvriers choisissent plus souvent une maternité en fonction de la proximité que les femmes de ménages cadres. Ces résultats illustrent la nécessité de lecture croisée entre accessibilité et statut socioéconomique (Socioeconomic status [SES] en anglais). De nombreux articles ayant pour étude de cas les Etats-Unis proposent ce type d’analyses croisant pathologie, SES et temps d’accès. Par exemple, le travail de Jia et al., (@jia2019), sur les différences d’accès à l’hôpital en Floride atteste que les patients des codes postaux riches et ruraux tendent à parcourir des distances plus grandes, ainsi que les jeunes, les blancs et les personnes ayant une assurance médicale privée. Les distances plus longues pour les patients des zones rurales sont probablement liées au manque d'accès à des hôpitaux locaux mais pour les autres cela pourrait indiquer une plus grande mobilité ou une possibilité de choix non contrainte par la distance. La localisation spatiale des hôpitaux et l’organisation sociale dans les territoires peuvent aussi montrer (notamment pour le cas des Etats-Unis avec des hôpitaux présents dans les villes-centres où vivent une grande partie des populations les plus défavorisées) des résultats plus inattendus des différences d’accessibilité en fonction des catégories sociales.


<br>


# Environnement méthodologique de l’étude

## Démarche reproductible
L’implémentation des méthodes a été réalisée dans un souci de reproductibilité. En ce sens, la préparation des données et leur analyse suit une chaîne de traitements reproductible avec le logiciel R.

Dans un souci de lisibilité et de synthèse, nous ne présentons ici que les objectifs des traitements, leur code associé et commenté ainsi que les résultats statistiques et graphiques qui en découlent. Pour leur interprétation thématique, se reporter aux publications **(ref à ajouter à la fin**) qui restituent et problématise les résultats les plus significatifs des explorations menées.

Cette démarche reproductible facilite les discussions autour des choix méthodologiques réalisés, mais rend aussi plus aisé une éventuelle mise à jour des analyses présentées ici.


## Géométries de référence
Cette étude mobilise des objets géographiques de différentes natures :

Hôpitaux : Il s'agit de points géolocalisés, géoréférencés depuis leur adresse avec la librairie banR. 

Mailles territoriales : Communes françaises (préciser version Admin Express utilisé).

Couches d'habillage cartographique : Rappeler les couches générées à partir des communes. 



## Données contextuelles

Ces données sont associées aux géométries pour répondre à la problématique

## L'offre en chirurgie
Ecrire ici quelque chose sur la procédure de collecte de données, les éléments importants à retenir... 

## Indicateurs socio-économiques
Ecrire ici quelque chose sur les données INSEE utilisées

## Temps d'accès
Ecrire ici quelque chose sur OSRM et les temps d'accès calculés. 

La description précise des sources, du libellé des indicateurs mobilisés et des conditions d'utilisation est disponible dans la partie [Métadonnées](_site/03_data.html).


<br>


# Les publications issues du projet

- [*Chirurgie et rétraction de l'accès à l'offre d'hospitalisation en France entre 2000 et 2018 : analyse des inégalités socio-spatiales*](valo/ASRDLF_2021_chirurgie et retraction_vf-1.pdf), Colloque de l'Association de Science Régionale de Langue Française, 1-3 septembre 2021, Avignon. 
- [*Éloignement de l'offre d'hospitalisation pour des soins de chirurgie en France : une comparaison public/privé par les distances temps](valo/RFTM_3_6_2021.pdf), 3èmes Rencontres Francophones Transport Mobilité, 2-4 juin 2021, Marne-la-Vallée.  


<br>

# Bibliographie

<span style="color:white">@baillot2012, @barlet2012, @grasland1990, @mudd2019, @pilkington2012, @combier2007</span>




[^1]: IRDES, Paris, France
[^2]: Université de Paris, UMS RIATE, CNRS, F-75013 Paris, France
[^3]: FR CIST, CNRS, F-73013 Paris, France
[^4]: Université de Paris, UMR Géographie-cités, F-75013 Paris, France
[^5]: ENPC, Paris, France
